package hello.controller;

import java.math.BigInteger;
import java.security.MessageDigest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
public class TestController {

	static String publicKey = "32c30a7a2bec1bc04013bf95d3bcb161";
	static String privateKey = "d1ffd909861b671991195ccd09b27013b3f787ce";
	static String timestamp = "1";
	
	
	@RequestMapping("/greeting")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="José") String name, Model model) {
        model.addAttribute("name", name);
        
        String request = "http://gateway.marvel.com/v1/public/characters?ts="+ timestamp+"&apikey="+ publicKey +"&hash=" + calcularMD5();
        System.err.println(request);
        
        RestTemplate restTemplate = new RestTemplate();
        String marvel = restTemplate.getForObject(request, String.class);
        model.addAttribute("marvel", marvel);
        
        System.out.println("MARVEL________________________________\n" + marvel);
        
        return "greeting";
    }
	
	private String calcularMD5(){
		String stringToHash = timestamp + privateKey + publicKey;
		String hashtext = "";
		
		try{
			MessageDigest m = MessageDigest.getInstance("MD5");
			m.reset();
			m.update(stringToHash.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			hashtext = bigInt.toString(16);
			
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
		}
		catch(Exception e){
			System.err.println("Erro ao Gerar o Hash!!!!!");
		}
		
		return hashtext;
	}
	
}
